from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("show_project", id=list.id)
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/task_create.html", context)


@login_required
def task_list(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": task,
    }
    return render(request, "tasks/task_list.html", context)
